---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Network]

@snap[west headline text-white span-70]
Network<br>*101*
@snapend

---
@title[Contents]

### Main areas of coverage

<br><br>

1. Bandwidth and Latency.
1. TCP , UDP , TLS and all that stuff
1. OSI layer model primer
1. Wireless and mobile Network and all the 'G'
1. HTTP, HTTP2 , web performance
1. browser stuff : webRTC, websockets, server sent events

<br><br>


---
@title[Tip! Bandwidth vs Latency]

<br>
What is Bandwidth , will increasing bandwidth improve latency??

---?include=template/md/bandwidthlatency/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---
@title[The Template Docs]

@snap[west headline span-100]
GitPitch<br>*The Template @css[text-orange](End) ;)*
@snapend

@snap[south docslink span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend
